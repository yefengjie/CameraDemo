# CameraDemo

#### 介绍
Camera Audio Demo

#### 目录介绍
| 目录                         |  功能描述     |
| ---------------------- | ------------------ |
| [CameraXBasic](https://gitee.com/chenjim/CameraDemo/tree/master/CameraXBasic) |  CameraX APIs如何使用By Kotlin.参考自 [Github Google camera-samples](https://github.com/android/camera-samples/tree/master/CameraXBasic) |
| [Camera2VideoJava](https://gitee.com/chenjim/CameraDemo/tree/master/Camera2VideoJava) | 用 Camera2 API 和 MediaRecorder 录制视频，并提取实时帧，参考自 [Github Google camera-samples](https://github.com/android/camera-samples/tree/master/Camera2VideoJava)  |
| [DualAudioRecord](https://gitee.com/chenjim/CameraDemo/tree/master/DualAudioRecord) | 使用AudioRecord同时录制两路音频，此示例可以用来检测设备是否支持  |
| [DualCamera](https://gitee.com/chenjim/CameraDemo/tree/master/DualCamera) | 同时打开两个Camera并通过onPreviewFrame获取YUV数据回调 |
| [OpenGLRecorder](https://gitee.com/chenjim/CameraDemo/tree/master/OpenGLRecorder) | Camera 通过OpenGL添加水印并录制视频 |

