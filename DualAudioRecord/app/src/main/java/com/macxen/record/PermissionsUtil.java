package com.macxen.record;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

/**
 * Created by jim.chen on 2018/7/18.
 */
public class PermissionsUtil {

    public static boolean doRequestPermissions(Activity activity) {
        boolean hasAllPermission = true;
        if (Build.VERSION.SDK_INT > 22) {
            String[] permissions = new String[]{
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.CAMERA,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            };

            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity, permissions, 1000);
                    hasAllPermission = false;
                    break;
                }
            }
        }

        return hasAllPermission;
    }
}
