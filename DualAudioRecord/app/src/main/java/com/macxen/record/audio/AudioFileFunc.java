package com.macxen.record.audio;

import android.media.MediaRecorder;


class AudioFileFunc {
    public static final int AUDIO_SAMPLE_RATE = 16000;
    public static final int AUDIO_INPUT = MediaRecorder.AudioSource.MIC;
}
