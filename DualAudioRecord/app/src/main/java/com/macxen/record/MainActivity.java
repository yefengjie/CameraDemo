package com.macxen.record;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.macxen.record.audio.ChatAudioRecord;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PermissionsUtil.doRequestPermissions(this);


        findViewById(R.id.action1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                throw new NullPointerException();
                ChatAudioRecord record = new ChatAudioRecord();
                record.startRecordAndFile("1111");
            }
        });


        findViewById(R.id.action2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatAudioRecord record = new ChatAudioRecord();
                record.startRecordAndFile("2222");
            }
        });
    }
}
