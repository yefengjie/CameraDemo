package com.macxen.record;

import android.app.Application;

/**
 * Created by jim.chen on 2018/10/13.
 */
public class UserApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Loger.init(this,true,false);
    }
}
